package com.red.nem12.exception;

import com.red.nem12.common.Constants;

public class NoStartRecordFoundException extends NEM12Exception {

	private static final long serialVersionUID = 5890392100710273228L;

	public NoStartRecordFoundException() {
		super("No record type " + Constants.RECORD_TYPE_START_OF_RECORDS + " found in the file.");
	}

}
