package com.red.nem12.exception;

public class InvalidQualityException extends NEM12Exception {

	private static final long serialVersionUID = 7381318451005121522L;

	public InvalidQualityException(String invalidQuality) {
		super("Invalid Quality " + invalidQuality);
	}
}
