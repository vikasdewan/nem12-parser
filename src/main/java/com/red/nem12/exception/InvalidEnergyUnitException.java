package com.red.nem12.exception;

public class InvalidEnergyUnitException extends NEM12Exception {

	private static final long serialVersionUID = 4213346455779526405L;

	public InvalidEnergyUnitException(String invalidEnergyUnit) {
		super("Invalid Energy Unit " + invalidEnergyUnit);
	}
}
