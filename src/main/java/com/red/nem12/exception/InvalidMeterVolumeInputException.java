package com.red.nem12.exception;

public class InvalidMeterVolumeInputException extends NEM12Exception {

	private static final long serialVersionUID = 758158865883111852L;

	public InvalidMeterVolumeInputException(String invalidMeterVolumeInput) {
		super("Invalid record for meter volume " + invalidMeterVolumeInput);
	}
}
