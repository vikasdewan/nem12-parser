package com.red.nem12.exception;

public class InvalidMeterReadInputException extends NEM12Exception {

	private static final long serialVersionUID = -5963881874495016754L;

	public InvalidMeterReadInputException(String invalidMeterReadInput) {
		super("Invalid record for meter reading " + invalidMeterReadInput);
	}
}
