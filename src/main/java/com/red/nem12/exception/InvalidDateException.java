package com.red.nem12.exception;

public class InvalidDateException extends NEM12Exception {

	private static final long serialVersionUID = 8259463529411145072L;

	public InvalidDateException(String message) {
		super(message);
	}
}
