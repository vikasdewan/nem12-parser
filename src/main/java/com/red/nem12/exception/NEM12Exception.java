package com.red.nem12.exception;

public class NEM12Exception extends RuntimeException {

	private static final long serialVersionUID = -6950202797091933697L;

	public NEM12Exception(String message) {
		super(message);
	}

	
}
