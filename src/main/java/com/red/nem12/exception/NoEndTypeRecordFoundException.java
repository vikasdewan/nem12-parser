package com.red.nem12.exception;

import com.red.nem12.common.Constants;

public class NoEndTypeRecordFoundException extends NEM12Exception  {

	private static final long serialVersionUID = 9034949115147455882L;

	public NoEndTypeRecordFoundException() {
		super("No record type " + Constants.RECORD_TYPE_END_OF_RECORDS + " found in the file.");
	}

}
