package com.red.nem12.exception;

public class NoFileSpecifiedException extends NEM12Exception {

	private static final long serialVersionUID = 8885381272844853058L;

	public NoFileSpecifiedException() {
		super("No input file specified. Please specify input file in the argument.");
	}

}
