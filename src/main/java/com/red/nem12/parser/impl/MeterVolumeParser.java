package com.red.nem12.parser.impl;

import java.math.BigDecimal;

import com.red.nem12.common.Constants;
import com.red.nem12.exception.InvalidMeterVolumeInputException;
import com.red.nem12.exception.InvalidQualityException;
import com.red.nem12.model.MeterVolume;
import com.red.nem12.model.Quality;

public class MeterVolumeParser {

	public MeterVolume parse(String meterVolumeAsStr)
	{
		if(null == meterVolumeAsStr || !meterVolumeAsStr.matches(Constants.VALID_METER_VOLUME_REGEX)) {
			throw new InvalidMeterVolumeInputException(meterVolumeAsStr);
		}

		String []meterVolumeAsArr = meterVolumeAsStr.split(Constants.METER_VOLUME_DELIMITER);
		BigDecimal volume = new BigDecimal(meterVolumeAsArr[2]);
		String qualityAsStr = meterVolumeAsArr[3];
		validateQuality(qualityAsStr);

		return new MeterVolume(volume, Quality.getValueOf(qualityAsStr));
	}

	private void validateQuality(String qualityAsStr)
	{
		if(Quality.getValueOf(qualityAsStr) == null)
		{
			throw new InvalidQualityException(qualityAsStr);
		}
	}
}
