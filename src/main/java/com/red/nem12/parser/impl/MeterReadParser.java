package com.red.nem12.parser.impl;

import com.red.nem12.common.Constants;
import com.red.nem12.exception.InvalidEnergyUnitException;
import com.red.nem12.exception.InvalidMeterReadInputException;
import com.red.nem12.model.EnergyUnit;
import com.red.nem12.model.MeterRead;

public class MeterReadParser {

	public MeterRead parse(String meterReadAsStr)
	{
		if(null == meterReadAsStr || !meterReadAsStr.matches(Constants.VALID_METER_READING_REGEX)) {
			throw new InvalidMeterReadInputException(meterReadAsStr);
		}

		String []meterReadAsArr = meterReadAsStr.split(Constants.METER_RECORD_DELIMITER);
		String nmi = meterReadAsArr[1];
		String energyUnit = meterReadAsArr[2];
		
		validateEnergyUnit(energyUnit);
		
		return new MeterRead(nmi, EnergyUnit.getValueOf(energyUnit));
	}

	private void validateEnergyUnit(String energyUnit)
	{
		if(EnergyUnit.getValueOf(energyUnit) == null)
		{
			throw new InvalidEnergyUnitException(energyUnit);
		}
	}
	
}
