package com.red.nem12.parser.impl;

import java.time.DateTimeException;
import java.time.LocalDate;

import com.red.nem12.common.Constants;
import com.red.nem12.exception.InvalidDateException;

public class DateParser {

	public LocalDate parse(String dateAsStr)
	{
		validateDate(dateAsStr);
		
		int year = Integer.valueOf(dateAsStr.substring(0, 4));
		int month = Integer.valueOf(dateAsStr.substring(4,6));
		int day = Integer.valueOf(dateAsStr.substring(6,8));

		return getLocalDate(year, month, day);
	}

	private LocalDate getLocalDate(int year, int month, int day) {
		try {
			return LocalDate.of(year, month, day);
		} catch (DateTimeException dateTimeException) {
			throw new InvalidDateException(dateTimeException.getMessage());
		}
	}

	private void validateDate(String dateAsStr)
	{
		if(null == dateAsStr || !dateAsStr.matches(Constants.VALID_DATE_FORMAT_REGEX)) {
			throw new InvalidDateException("Invalid date " + dateAsStr);
		}
	}
}
