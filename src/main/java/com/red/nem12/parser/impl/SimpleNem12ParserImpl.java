package com.red.nem12.parser.impl;

import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.red.nem12.common.Constants;
import com.red.nem12.exception.NEM12Exception;
import com.red.nem12.exception.NoEndTypeRecordFoundException;
import com.red.nem12.exception.NoStartRecordFoundException;
import com.red.nem12.io.CSVReader;
import com.red.nem12.io.Reader;
import com.red.nem12.model.MeterRead;
import com.red.nem12.model.MeterVolume;
import com.red.nem12.parser.SimpleNem12Parser;

public class SimpleNem12ParserImpl implements SimpleNem12Parser {

	private List<String> readLines;

	@Override
	public Collection<MeterRead> parseSimpleNem12(File simpleNem12File) {

		Reader reader = new CSVReader();
		readLines = reader.read(simpleNem12File.getAbsolutePath());

		if (readLines != null && !readLines.isEmpty()
				&& !readLines.get(0).equalsIgnoreCase(Constants.RECORD_TYPE_START_OF_RECORDS)) {
			throw new NoStartRecordFoundException();
		}

		if (readLines != null && !readLines.isEmpty()
				&& !readLines.get(readLines.size() - 1).equalsIgnoreCase(Constants.RECORD_TYPE_END_OF_RECORDS)) {
			throw new NoEndTypeRecordFoundException();
		}

		return readMeterRecords();
	}

	private List<MeterRead> readMeterRecords() {

		List<MeterRead> meterReads = new ArrayList<>();

		for (int idx = 1; idx < readLines.size() - 1; idx++) {
			String record = readLines.get(idx);
			try {
				if (record.startsWith(Constants.RECORD_TYPE_METER_READ)) {
					MeterRead meterRead = new MeterReadParser().parse(record);
					readAndSetVolume(idx + 1, meterRead);
					meterReads.add(meterRead);
				}
			} catch (NEM12Exception nem12Exception) {
				//skip the bad meter reading.
				System.err.println("Unable to parse record [" + record + "], Error : " + nem12Exception.getMessage());
			}
		}
		return meterReads;
	}

	private void readAndSetVolume(int volumeIdx, MeterRead meterRead) {

		for (; volumeIdx < readLines.size() - 1; volumeIdx++) {

			String record = readLines.get(volumeIdx);
			try {
				if (!record.startsWith(Constants.RECORD_TYPE_METER_VOLUME)) {
					break;
				}
				MeterVolume meterVolume = new MeterVolumeParser().parse(record);
				LocalDate meterReadDate = new DateParser().parse(record.split(Constants.METER_VOLUME_DELIMITER)[1]);
				meterRead.appendVolume(meterReadDate, meterVolume);

			} catch (NEM12Exception nem12Exception) {
				// skip the bad volume record.
				System.err.println("Unable to parse record [" + record + "], Error : " + nem12Exception.getMessage());
			}
		}
	}
}
