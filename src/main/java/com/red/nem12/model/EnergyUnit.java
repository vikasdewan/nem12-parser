// Copyright Red Energy Limited 2017

package com.red.nem12.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents the KWH energy unit in SimpleNem12
 */
public enum EnergyUnit {

	KWH;

	private static Map<String, EnergyUnit> map = new HashMap<>();

	static {
		for (EnergyUnit energyUnitEnum : EnergyUnit.values()) {
			map.put(energyUnitEnum.toString(), energyUnitEnum);
		}
	}

	public static EnergyUnit getValueOf(String energyUnit) {
		return map.get(energyUnit);
	}
}
