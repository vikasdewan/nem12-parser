// Copyright Red Energy Limited 2017

package com.red.nem12.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents meter read quality in SimpleNem12
 */
public enum Quality {

	A, E;

	private static Map<String, Quality> map = new HashMap<>();

	static {
		for (Quality qualityEnum : Quality.values()) {
			map.put(qualityEnum.toString(), qualityEnum);
		}
	}

	public static Quality getValueOf(String quality) {
		return map.get(quality);
	}
}