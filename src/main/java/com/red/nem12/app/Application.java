package com.red.nem12.app;

import java.io.File;
import java.util.Collection;

import com.red.nem12.exception.NEM12Exception;
import com.red.nem12.exception.NoFileSpecifiedException;
import com.red.nem12.model.MeterRead;
import com.red.nem12.parser.impl.SimpleNem12ParserImpl;

public class Application {

	public static void main(String[] args) {

		File simpleNem12File;

		try {
			if (args.length == 0 || args[0] == null) {
				throw new NoFileSpecifiedException();
			}

			simpleNem12File = new File(args[0]);
			Collection<MeterRead> meterReads = new SimpleNem12ParserImpl().parseSimpleNem12(simpleNem12File);

			for (MeterRead meterRead : meterReads) {
				System.out.println("Total volume for Meter " + meterRead.getNmi() + " is " + meterRead.getTotalVolume());
			}
		} catch (NEM12Exception e) {
			System.err.println(e.getMessage());
		} catch (Exception e) {
			System.err.println("Error occurred " + e.getMessage());
		}
	}
}
