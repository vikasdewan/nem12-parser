package com.red.nem12.io;

import java.util.List;

public abstract class Reader {

	public abstract List<String> read(String fileName); 
	
}
