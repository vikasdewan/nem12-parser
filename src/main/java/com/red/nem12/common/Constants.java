package com.red.nem12.common;

public class Constants {

	public static final String METER_RECORD_DELIMITER = ",";
	public static final String METER_VOLUME_DELIMITER = ",";
	public static final String RECORD_TYPE_START_OF_RECORDS = "100";
	public static final String RECORD_TYPE_METER_READ = "200";
	public static final String RECORD_TYPE_METER_VOLUME = "300";
	public static final String RECORD_TYPE_END_OF_RECORDS = "900";
	
	public static final String VALID_METER_READING_REGEX = "(" + RECORD_TYPE_METER_READ + ",)[a-zA-Z0-9]{10}(,)[a-zA-Z]+";
	public static final String VALID_METER_VOLUME_REGEX = "(" + RECORD_TYPE_METER_VOLUME +",)[0-9]{8}(,)[-]?[0-9]+[.]?[0-9]*(,)[a-zA-Z]+";
	public static final String VALID_DATE_FORMAT_REGEX = "^([1-2][0-9])?[0-9][0-9](0[1-9]||1[0-2])(0[1-9]||[1-2][0-9]||3[0-1])$";
}
