package com.red.nem12.parser.impl;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.red.nem12.exception.InvalidMeterVolumeInputException;
import com.red.nem12.exception.InvalidQualityException;
import com.red.nem12.model.MeterVolume;

public class MeterVolumeTest {

	private MeterVolumeParser parser;
	private MeterVolume meterVolume;
	private String meterVolumeAsStr;

	@Before
	public void setUp() throws Exception {
		parser = new MeterVolumeParser();
	}

	@After
	public void tearDown() throws Exception {
		parser = null;
		meterVolume = null;
		meterVolumeAsStr = null;
	}

	@Test
	public void shouldParseMeterVolume() {
		meterVolumeAsStr = "300,20161115,32.0,A";
		meterVolume = parser.parse(meterVolumeAsStr);
		
		assertEquals("32.0", meterVolume.getVolume().toString());
		assertEquals("A", meterVolume.getQuality().toString());
	}

	@Test(expected = InvalidMeterVolumeInputException.class)
	public void shouldThrowErrorWhenNullIsPassed(){
		parser.parse(meterVolumeAsStr);
	}

	@Test(expected = InvalidMeterVolumeInputException.class)
	public void shouldThrowErrorWhenInvalidInputIsPassed(){
		meterVolumeAsStr = "300,20161115,ABC,A";
		parser.parse(meterVolumeAsStr);
	}

	@Test(expected = InvalidQualityException.class)
	public void shouldThrowErrorWhenInvalidEnergyUnitIsPassed(){
		meterVolumeAsStr = "300,20161115,32.0,Z";
		parser.parse(meterVolumeAsStr);
	}

}
