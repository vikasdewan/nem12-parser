package com.red.nem12.parser.impl;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.red.nem12.exception.InvalidDateException;

public class DateParserTest {

	private DateParser parser;
	private String dateAsStr;
	private LocalDate date;
	
	@Before
	public void setUp() throws Exception {
		parser = new DateParser();
	}

	@After
	public void tearDown() throws Exception {
		parser = null;
		dateAsStr = null;
		date = null;
	}

	@Test
	public void shouldParseDate() {
		dateAsStr = "20160228";
		date = parser.parse(dateAsStr);
		
		assertEquals(2016, date.getYear());
		assertEquals(02, date.getMonthValue());
		assertEquals(28, date.getDayOfMonth());
	}

	@Test(expected = InvalidDateException.class)
	public void shouldThrowErrorWhenInvalidInputIsPassed() {
		dateAsStr = "yyyymmdd";
		date = parser.parse(dateAsStr);
	}

	@Test(expected = InvalidDateException.class)
	public void shouldThrowErrorWhenNullIsPassed() {
		date = parser.parse(dateAsStr);
	}

	@Test(expected = InvalidDateException.class)
	public void shouldThrowErrorWhenInvalidDateIsPassed() {
		dateAsStr = "20160230";
		date = parser.parse(dateAsStr);
	}
}
