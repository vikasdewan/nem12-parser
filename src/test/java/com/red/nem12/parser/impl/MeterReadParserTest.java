package com.red.nem12.parser.impl;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.red.nem12.exception.InvalidEnergyUnitException;
import com.red.nem12.exception.InvalidMeterReadInputException;
import com.red.nem12.model.MeterRead;

public class MeterReadParserTest {

	private MeterReadParser parser;
	private MeterRead meterRead;
	private String meterReadAsStr;

	@Before
	public void setUp() throws Exception {
		parser = new MeterReadParser();
	}

	@After
	public void tearDown() throws Exception {
		parser = null;
		meterRead = null;
		meterReadAsStr = null;
	}

	@Test
	public void shouldParseMeterRead() {
		meterReadAsStr = "200,6123456789,KWH";
		meterRead = parser.parse(meterReadAsStr);

		assertEquals("KWH", meterRead.getEnergyUnit().toString());
		assertEquals("6123456789", meterRead.getNmi());
	}

	@Test(expected = InvalidMeterReadInputException.class)
	public void shouldThrowErrorWhenNullIsPassed(){
		parser.parse(meterReadAsStr);
	}

	@Test(expected = InvalidMeterReadInputException.class)
	public void shouldThrowErrorWhenInvalidInputIsPassed(){
		meterReadAsStr = "200,INVALID,KWH";
		parser.parse(meterReadAsStr);
	}

	@Test(expected = InvalidEnergyUnitException.class)
	public void shouldThrowErrorWhenInvalidEnergyUnitIsPassed(){
		meterReadAsStr = "200,1234567890,WATT";
		parser.parse(meterReadAsStr);
	}
}
