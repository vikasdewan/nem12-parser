#Description

This is a NEM12 application that reads the NEM12 dump file and extracts the energy consumption for a particular meter.

#Instructions
This section contains the information about software requirements and running the application.

##Software Requirements
1. Java 8
2. Maven
3. Test Cases require JUnit

##How to run application ?

1. Download the code from bitbucket and navigate to the project directory.
2. To build the project run "mvn clean install"
3. Executable JAR will be created in the "target" folder under project directory.
4. Navigate to target folder.
5. Run the command in following format "java -jar nem12parser-0.0.1-SNAPSHOT.jar <INPUT_FILE_LOCATION>".
6. Sum of volumes will be displayed on the console.